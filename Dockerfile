FROM golang:1.16-alpine
RUN apk add --no-cache git && \
        git clone https://github.com/betauia/Beetroot.git
WORKDIR ./Beetroot/cmd/beetroot
EXPOSE 8080
RUN go get .
CMD go run main.go

#FROM golang:alpine
#COPY --from=build go/Beetroot/cmd/beetroot/beet .
#EXPOSE 8080
#CMD [ "./beet" ]